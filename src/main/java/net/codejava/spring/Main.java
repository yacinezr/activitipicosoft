package net.codejava.spring;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import net.codejava.spring.model.Person;
import net.codejava.spring.service.MyService;
import org.activiti.engine.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import java.util.List;

public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Date date = new Date();

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        MyService personService = (MyService) ctx.getBean("myService");
        //personService.createPersons();

        Person si = new Person();
        si.setName("yacine");
        si.setBirthDate(date);
        personService.saveAndFlush(si);

        Person si1 = new Person();
        si1.setName("jiji");
        si1.setBirthDate(date);
        personService.saveAndFlush(si1);


        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("activit.cfg.xml");
        RepositoryService repositoryService = (RepositoryService) applicationContext.getBean("repositoryService");
        String deploymentId = repositoryService.createDeployment().addClasspathResource("simple-process.bpmn20.xml").deploy().getId();

        ProcessEngine processEngine = (ProcessEngine) applicationContext.getBean("processEngine");

        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance1 = runtimeService.startProcessInstanceByKey("process_pool1");




    }
}